import matplotlib.pyplot as plt
import numpy as np

import pandas as pd
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

file = './out.csv'


df = pd.read_csv(file,header=None)


word_freq = {word: freq for word, freq in zip(df[0], df[1])}

wordcloud = WordCloud(width=800, height=600, background_color='white',max_words=200,max_font_size=80)
wordcloud.generate_from_frequencies(word_freq)

# Display the word cloud using matplotlib
plt.figure(figsize=(12, 10))
plt.imshow(wordcloud, interpolation='bilinear',cmap='bwr')
plt.axis('off')  # Remove axis
plt.show()
