#!/bin/bash
out=$(cat $1 |grep "[0-9][A-Z]" | sed 's/ /\n/g' | grep "[0-9][A-Z]" | sort |uniq -c | sort -n | awk '{print$2,","$1":"}')
cnt=$(cat $1 | grep "# "| wc -l)

echo $out | sed 's/:/\n/g'

no=$(echo $out | sed 's/:/\n/g' | wc -l)
echo 
echo "Stats"
echo "======================="
echo "total nets: $cnt"
echo "unique calls: $no"
# | awk '{print $2, $1}'|awk -F " " '{Total=Total+$2} END{print "Total is: " Total}'
