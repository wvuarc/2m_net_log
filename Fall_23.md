#  17/08/2023

1. N0IAN
2. KE8TJE
3. KE8NHN
4. KE8WZW
5. KB8CFD
6. WB1AAL
7. N1LVI - echolink
8. N3AMK - echolink
9. WV8EHB
10. KC3VRD

- end of net: 2124 h

# 24/08/2023

1. N0IAN - NC
2. KE8TJE - ANC
3. N3AMK
4. KC8NHN
5. WV8EHB
6. KE8WZW
7. N8OZY

- comments:
	- first meeting next week. 414 crossings
- end of net: 2117 h

# 31/08/2023

1. N3AMK - NC
2. N0IAN - ANC
3. WC0LE
4. KE8TJE
5. N8OZY
6. 

- comments: missing logs requested from NC/ANC
- end of net:

# 07/09/2023

1. N0IAN - NC
2. KE8TJE - ANC
3. WC0LE 
4. KE8WZW - Jeramy
5. KE8VEQ - Elizabeth
6. KE8KKC - Jordan
7. KC1SVN -
8. KC8YUI
9. KB8CFD

- comments: 
 - Oct 15th Alzhimers walk 14.00 h (1315 h check in and setup) email if needed :YUI
- end of net: 2121 h 

# 14/09/2021

1. KE8TJE - NC
2. N3AMK -ST
3. N0IAN - ST
4. WC0LE - ST
5. KC8NHN 
6. KE8WZW - ANC
7. WV8EHB
8. KE8KKC - Jorden
9. N8OZY 
10. KE8YYM
11. KC8YUI 
12. N8TJD 

- comments:
	- 7th Oct walk
		- out of the dark (for suiside awareness)
    - 1000 h registration 
    - Krep park (?)
	- 15th Oct walk
		- 1300 h at the old mall
		- 1.5 h max
- end of net: 2123 h 

# 21/09/2023

1. N0IAN - NC
2. KE8WZW - ANC
3. N3AMK
4. KC8NHN
5. KB8CFD
6. KE8TJE 
7. KE8ZDN
8. N8OZY
9. KC8YUI

# 28/09/2023

1. N0IAN - NC
2. KE8WZW - ANC
3. N3AMK - ST
4. KE8TJE - ST
5. KA3WVU
6. WC0LE
7. KE8VEQ
8. KE8KKC
9. KE8ZDN
10. N8OZY
11. KC8YUI

- comments:
	- visitation day 30/09
  - upcomming walks: 7th and 14th Oct


# 05/10/2023

1. KE8TJE - NC
2. N0IAN - ANC
3. WB1AAL
4. KC8YUI
5. N3AMK

- comments: 
  - Walks comming up
- end of net: 2120 h

# 12/10/2023

1. WC0LE - NC
2. KC8YUI - Reminder about the walk on sunday @ 1pm
3. KQ4LDS - First time checking into the net
4. KC3TCL - writing a lab report
5. N8HGL - Wanted to tell Hasith hello and that he made it to the net

- end of net: 2114

# 19/10/2023

1. N0IAN - NC
2. KE8TJE - ANC
3. WC0LE
4. KC8NHN
5. KE8WZW
6. N3AMK
7. KE8YWR - tristin ?
8. KX2A - Jan
9. WB1AAL
10. KC8YUI
11. KE8YYM - Chris (granvile)
12. N8HGL
13. K3JT - Terry

- comments:
  - organizing a TIAD on week ends. DITZIAN@gmail.com Jans email
  - 11/12/2023 Xmas parade
- end of net: 2132

# 26/10/2023

1. WC0LE - NC
2. KE8TJE - ANC
3. N0IAN -
4. KE8WZW -
5. WB1AAL - track back
6. KE8LMT - mike 
7. N3AMK - echolink
8. N8OZY
9. KA3WVU

- Comments:
 - track backs on yeasu radios
-  end of net: 2120 h

# 02/11/2023

1. N0IAN - NC
2. WC0LE - ANC
3. WV8EHB
4. N3AMK
5. KE8WZW
6. N8OZY
7. WA8TGG
8. KC3VAJ 
9. KE8VEQ


# 09/11/2023

1. N0IAN - NC
2. KE8WZW - ANC
3. N3AMK - ST
4. KE8TJE - ST
5. WC0LE - ST
6. KC8NHN
7. KA3WVU
8. KB8CFD
9. KC8YUI
10. WB1AAL
11. WA8TGG
12. WV8EHB
13. KC3VOP

- end of net: 2131 h

# 16/11/2023

1. N0IAN - NC
2. KE8WZW - ANC
3. KC3RXZ - ST
4. WV8EHB 
5. N3AMK
6. KE8TJE
7. WC0LE
8. WB1AAL
9. WA8TGG

- comments:
 - TIAD ~15 new Hams
- end of net: 2121 h

## Turkey net (11/23/2023)

1. N3AMK - NC
2. KE8TJE 
3. KC8YUI
4. KE8WZW - echolink 

- comments: Dec 11 th Xmas parade
- end of net: 2116 h

# 30/11/2023

1. N0IAN - NC
2. KE8WZW 
3. N3AMK 
4. KA3WVU
5. WC0LE
6. KB8CFD
7. KE8TJE
8. KC8YUI
9. KE8YWR - Tristin 
10. WA8TGG

- Comments: 
	- Xmas parade 11th 
- end of net: 2125 h

# 7/12/2023

1. WC0LE
2. KE8TJE
3. N0IAN
4. KE8WZW
5. K8FLR
6. AD8LQ
7. WA8TGG
8. KC3YBC
9. KA3WVU
10. N3AMK

- comments:
	- Xmas light antenna on ESB

# 14/12/2023

1. KE8TJE - NC
2. KE8WZW - ANC
3. N0IAN - ST
4. kE8TJF - 
5. WB1AAL - 
6. WA8TGG - 
7. KB8CFD
8. KC3VAJ

- comments:
	- MCR 70 cm has issues
	- end of net: 2116 h

# 21/12/2023

1. KE8TJE
2. N3AMK - ST
3. N3LIF - ST
4. WB1AAL - Jeff
5. WA8TGG - 
6. KE8YYM - Chris Gramville,
7. KE8WZW - Echolink
8. KX2A - Jan. Mount moris, K3S FT8 setting up issues
9. KA3WVU- Echolink

- Comments: 
	- road conditions not great
- End of net: 2117 h

# 28/12/2023

1. KE8TJE
2. N3AMK
3. KA3WVU
4. KE8WZW
5. WA8TGG

- Comments:
	- end of net: 2113

