# W8CUL : Traffic and information net log

- On the 146.760 MHz repeater.
- Pl-tone: 103.5 Hz

## Format and information entry

- Log the call sign and time of check in (New implementation)
- Log end of net time
- Log any comments and the sufix of the call along with that

## Getting statistics

Run `get_stats.sh <filename>` to print a list of calls with the count they have checked in.



