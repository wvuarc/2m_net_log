Unless other wise noted the default repeater is the school repeater (W8CUL)
MWA - used as a short hand for W8MWA repeater
MCR - K8MCR repeater
### Week 1

- 03.02.2025
	- 1540 h - Called out, KF8AQR came back, talked to him about the Merit badge university and the club offering Radio Merit badge we are doing on March 1st. 
- 04.02
	- 1040 h  - No reply
	- 1400 h - No reply
	- 2050 h - KE8YYM was looking for BMI,
		- Talked about ICOM 718, a new radio he is looking to buy
- 05.02
	- 1358 h - KE8WZW talk to about meeting up at the rec for the radio club table
- 08.02
	- 0845 h  KE8TJF directions and updates on getting to TIAD (Tech-in a Day our clubs version of teaching and getting people on the air)
	- 1045 helped folks demonstrate radio white hiking cooper's rock
- 10.02
	- 2105 h on W8MWA -  weekly ARES net checkin as short time(KC8YUI country EC-net controller)
- 11.02
	- 1755 h - KE8YYM talked with him a bit while walking back home
- 12.02
	- 0710 h - KC3YLF new Alinco in the car. Getting ready for an exam, writing down a formula sheet 
	- 1806 h - KC3YLF audio issues while driving a stick. No VOX in the CR10HT, and they don't give you a cable to program it
	- KA3WVU- Tyson visiting the shack for SCR and operate a bit
- 13.02
	- 1217 h - KE8YYM - Coordinated about the Slim jim build day 
	- 1233 h - KE8TUZ BOB - information about the location and time for the build day
	- 1645 h - KE8TUZ,KC3YLF,N3AMK coordinating the build day
- 14.02
	- 1904 h - KF8DKZ, Steve brand new ham who was at TIAD. First contact sent him a QSL card
- 15.02
	- 1425 h MWA - K2E** wrote down the call while in the shack, he was looking for VE information and wanted to help test. 
	- ~1800 h MWA - WB1AAL SKYWARN activation due to catastrophic flooding in southern WV (I did not talk but wanted to note it down for refe )
	- 1812 h MWA - KC3YLF up in Pittsburgh and have a good signal
- 16.02
	- 1450 h - SKYWARN checkin with weather and wind conditions
	- 1658 h - WV8RWQ came into town after the week end.
- 17.02
	- 1342 h - KE8TJF interview in Smith Fields got their early can hit the repeater with 50W not on low power 
- 20.02
	- 1118 h - KC3YLF just saying hi and commenting on the audio
- 21.02 
	- 1531 h - No reply
	- 1520 h - N3AMK talked about the sat setup
	- 1628 h - KC3YLF contact from Terra alta. (this is the longest Jack and I have made a contact on our rptr)
	- 1630 h KX8T - AB8E,KE8FVA on the ham talk
- 22.02
	- around noon - WV8RWQ came back for a test call, he had installed his radio in the car