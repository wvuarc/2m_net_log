

A [Slim Jim (link)](https://mdrc.org.au/wp-content/uploads/2020/12/IMG_1760-1024x768.jpg) is a rollable/flexible antenna made out of ladder line. This would be a good addition to your emergency toolkit as a ham to increase your reach in an emergency situation. This can be used with your hand held or with a mobile radio and often can handle up to 50 W.
## 1. Building instructions

Note: Following instruction are based on information available [here](https://m0ukd.com/calculators/slim-jim-and-j-pole-calculator/) and customized based on testing with the material available to us. The following figure will be used when providing dimensions in the following sections 

1. Select a section of ladder line (we are using a 450 $\Omega$)  at least about **157 cm** in length. the goal is to solder the top and bottom ends to make a big loop.
2. Trim, Bend and solder the top an bottom joints to match a total length (measurement A) of **154.4 cm**
	![[Pasted image 20240417231811.png]]
1. 


## 2. Tuning your antenna 




