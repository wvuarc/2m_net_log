# 2m_net_log

Logs from the weekly 2m net on Thirsday afternoons

## 18/08/22

1. KC3RXZ (NC)
1. WB1AAL (ANC)
1. KE8RVV
1. N8MDC
1. N0IAN
1. KE8TJE
1. KD8BMI
1. WA8TGG - Wednesday nets
1. KC8YUI - Foxhunt session on Staurday
1. WN3TYC

- End of net at 2123 h
 
## 25/08/22

1. KC3RXZ (NC)
1. N0IAN (ANC)
1. KE8RVV 
1. N8MDC
1. KC3UCE 
1. KE8TJE
1. KC3MDX
1. WV8EHB
1. KC8YUI - Saturday 27th about 9.00 AM - 03.00 PM (possible fox hunt) at coopers rock
1. WA8TGG

- Additional comments on the plant :P
- End of net at 2125 h

## 05/09/22

1. KC3RXZ
1. WB1AAL
1. KC3UCE
1. K8JFW 
1. KC2MDX
1. KE8TJE
1. N0IAN
1. KC8YUI - alzheimers march Oct 16th 1 PM old mall 
1. WA8TGG

- End of net 2120 h

## 15/09/22

- Net status unknow 

## 22/09/22

1. KC3RXZ (NC)
1. N8MDC (ANC)
1. KE8TJE
1. KC3MDX
1. N0IAN
1. KE8TCH
1. WA8YCD 
1. WA8TGG - More Fox hunting

- End of net 2115 h

## 29/09/22

1. N0IAN (NC)
1. WB1AAL (ANC)
1. KE8TJE 
1. WA8TGG
1. KD8BMI
1. WM3TYZ
- end of net 2115 h

## 06/10/22

1. NOIAN (NC)
2. WB1AAL (ANC) - Director networking WVU at one time
3. KE8RVV - Micel
4. KE8TDM 
5. KE8TJE
6. KE8UEY - Jason
7. WA8TGG - Peter (simplex 9.00 PM,146.520 MHz FM, 144.200 MHz USB)
- End of net 2116 h

## 13/10/22 
 
 1. N8MDC 
 2. KE8TJE - 
 3. KE8RVV - Tech in a Day 22/FM build day 28
 4. KE8TUZ
 5. WA8TGG -

##  20/10/22

1. KC3RXZ (NC)
2. WB1AAL (ANC)
3. N0IAN
4. KE8TJE 
5. WV8EHB
6. N3AMK
7. KE8TUZ
8. KE8UEY
9. WA8TGG
10. W93TYC
11. KD8BMI

- End of net 2121 h

## 27/10/22

1. KC3RXZ - NC
2. KE8TJE - ANC
3. WN3TYC - Shorttime
4. KE8TJF 
5. KE8RVV
6. KE8TDM
7. WB1AAL
8. KE8TUZ
9. WA8TGG
10. N8MDC

- Comments : 7.30 AM 432.100 USB/CW contact. ANC took up the net during closing since NC was not available 
- End of net  2126 h

## 03/11/22

1. KC3RXZ
2. N0IAN
3. KE8RVV
4. KC3UCE
5. KE8TJF
6. KE8TJE
7. KE8VWU
8. KE8VWQ
9. KE8TUZ
10. KE8UEY
11. N8MDC
12. WN3TYC

- Comments : 12/13 possible TIAD
- End of net 2131 h

## 10/11/22

1. KE8TJE - NC
2. N0IAN - ANC
3. N8MDC 
4. KE8TJF
5. WV8EHB
6. WB1AAL
7. N8OZY - 5th use of repeater for christmas parade

- Comments: 12/11 TIAD
- End on net 2123 h

## 01/12/2022

1. KC3RXZ - NC
2. N0IAN - ANC
3. N8MDC
4. KE8TJF
5. KE8TJE
6. KE8RVV
7. KE8VEQ
8. WB1AAL
9. WA8TGG
10. KE8VWQ
11. KE8VWU 

- Comments: 
- End of net: 2123 h

## 08/12/2022

1. KC3RXZ - NC
2. N0IAN - ANC
3. N8MDC
4. KA3WVU 
5. KE8TJE
6. WV8EHB
7. WB1AAL
8. WA8TGG

- comments: xmas party 17th and 19th, 10 th 20.00 PM 10$ limit for gift 
- End of net:  

## 15/12/2022

1. KC3RXZ - NC
2. N0IAN - ANC
3. WB1AAL
4. WA8TGG

- comments : 70cm is back online and DMR repeater (should) be back online
- End of net: NA
