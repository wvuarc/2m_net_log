
# 01/12/23

1. N0IAN - NC
2. KE8TJE - ANC
3. K8JFW - 
4. N8OZY
5. WB1AAL
6. WA8TGG
7. KD8TYQ - Charles 
8. WC0LE - 
9. N1LVI

- Comments : MWA meeting 17th, MCAR meeting 21st Saturday
- End of net 2123 h

# 01/19/23

1. KC3RXZ - NC
2. KE8TJE - ANC
3. N0IAN
4. KA3WVU
5. K8JFW
6. WC0LE
7. N1LVI
8. WB1AAL - Jeff
9. N8OCY
10. KD8NZX - Alex
11. WA8TGG - Pete

- Comments : MCA POTA and foxhunts in Spring
- VHF contest this weekend. Sat evening - Sunday evening  

# 01/26/2023

1. KE8TJE - NC
2. N0IAN - ANC
3. WC0LE
4. K8JFW
5. KE8UEY
6. NILVI
7. WA8TGG

- Comments: close of net at 21.22 h

# 02/02/2023

1. KC3RXZ - NC
2. KE8TJE - ANC
3. N0IAN
4. KA3WVU
5. WC0LE
6. N3AMK
7. N8MDC
8. WB1AAL
9. KC8YUI
10. WA8TGG - 
11. KD8BMI - Allem
12. N1LVI - Bruce
13. WN3TYC - Tye
14. KE8VEQ 


- comments : 
    - N3SH.org hamfest
    - Fox hunt on April 29th - South park
    - Field day
    - 15/10 good propergation Alaska on 10 m earlier 
    - 29.600 FM activity on there

# 02/09/2023
1. WC0LE - NC
2. WB1AAL - ANC
3. KE8TJE - ST
4. N0IAN
5. N3AMK - ST
6. KE8VEQ
7. KC3TCM 
8. N1LVI
9. WA8TGG
10. N8MDC
11. <NA>
12. W8TES

- end of net: 2123 h

# 02/16/2023

1. KC3RXZ - NC
2. KE8TJE - ANC
3. WC0LE - ST
4. N0IAN
5. KA3WVU
6. KB8CFD
7. K8JFW
8. N3AMK
9. KE8VWQ
10. WA8TGG
11. WB1AAL
12. KD8NZX
13. N8OZY
14. N1LVI

- Comments: 
    - wireless x repeater on 70 cm repeater + net 

# 02/23/2023

1. KC3RXZ - NC
2. KE8TJE - ANC
3. K8JFW - Shotr time 
4. N0IAN
5. WB1AAL
6. N1LVI
7. N3AMK
8. KE8VEQ
9. WC0LE

- comments: 
    - LVI leaving requested echolink setup.
    - TJE remarks + echo link comments
- end of net: 2129 h

# 03/02/2023

1. N0IAN - NC
2. KE8TJE - ANC
3. KB8CFD
4. N3AMK
5. WC0LE
6. KC3RXZ
7. KE8VWQ
8. KC3VOO
9. KA3WVU
10. KE8VEQ
11. KD8BMI
12. KC8YUI
13. WB1AAL
14. KC3VOP

- Comments: 
    - YUI: email KC8YUI@arrl.net on fox hunts or EFHW
    - BMI: TGIF DMR net on TG 4163
- end of net: 2135 h

# 03/09/2023

1. KC3RXZ - NC
2. KE8TJE - ANC
3. N0IAN
4. N8MDC
5. KA3WVU
6. KB8CFD
7. W8TES
8. KE8VEQ
9. WC0LE
10. KC3VOO
11. KE8GBQ
12. KC8YUI
13. KD8NZX
14. WB1AAL
15. KC3VOP
16. N3AMK
17. N1LVI

- comments: 
    - Running echolink
    - Antenna build day is in the works
- End of net: 2142 h

# 03/16/2023

1. KE8TJE - NC
2. KC8YUI - ANC
3. N3AMK - Echolink(ST)
4. WB1AAL 

- comments:
    - old radios
    - Charlston Ham fest. Jim is going and I am joining him to check it out
    - Echolink is up now
- end of net: 2117 h

# 03/23/2023

1. N0IAN - NC
2. KE8TJE - ANC
3. WC0LE
4. N8MDC
5. KE8VEQ
6. N3AMK - Echo link
7. WB1AAL
8. KE8LJL

- comments :
    - New officers were elected and congratulations for them
    - Jeff is using his ST20T 80's radio and it works
    - Jeff thanked the club for the DMR repeater
    - KE8WOL late check I made a contact
- End of net: 2120 h

# 03/30/2023

1. KE8TJE - NC
2. N0IAN - ANC
3. N3AMK - ST
4. KE8VEQ
5. N8OZY
6. KB8AOB - Rick (new station)
7. WB1AAL
8. WA8TGG
9. KC8YUI

- comments : 
    - Meeting next week
    - Fox hunt, support 
-End of net: 2125 h

# 04/06/2023

1. KE8TJE - NC
2. N0IAN - ANC
3. WC0LE
4. WB1AAL
5. KD8NZX 

- comments: 
    - send info to alex about the location
    - TIAD 15th 
    - Antenna build 21st 5.00 PM
    - Fox hunt 29th 10.00 AM
- end of net : 2123 h

# 04/13/2023

1. KE8TJE
2. KC3RXZ
3. N0IAN
4. K8JFW
5. KE8WZW
6. WB1AAL
7. N3AMK
8. N8TJD - Greg 
9. WC0LE

- end of net: 2128 h

# 04/20/2023

1. KE8TJE
2. N1LVI
3. N0IAN
4. KA3WVU
5. K8JFW
6. KE8WZW
7. KC8WDP - No recheck
8. N8TJG -Greg
9. WB1AAL
10. N8OZY
11. WA8TGG
12. KC3RXZ

- end not recorded
- comments
	- build day 21st in the Hub

# 04/27/2023

1. N0IAN
2. WB1AAL
3. KE8VEQ
4. K8JFW
5. KE8WZW

- Echo link: RX only N3AMK,KE8TJE
- end of net : 2117 h


# 05/04/2023

1. KC3RXZ
2. KE8TJE
3. N0IAN
4. N3AMK
5. N8OZY
6. KC8YUI 
7. WB1AAL
8. KB8CFD
9. KE8WZW

- comments:
	- Fox hunt
		- 145.650 
		- 147.570
		- 147.455
	- Gabe signing off remarks
	- email Jim about field day
- end of net: 2136 h
 
