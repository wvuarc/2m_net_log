# 11/05/2023

1. KE8TJE - NC
2. N8MDC - ST
3. KE8LJL
4. KC8YUI 

- comments: 
	- foxhunt on field day
	- 3 club field day this time too
- end of net : 2114 h


# 18/05/2023

1. KE8TJE
2. KC8YUI

- comments:
	- filed day will be using software logging

# 23/05/2023

1. KE8TJE
2. WB1AAL
3. WC0LE
4. KC8YUI

- end of net: 2117 h
- comments:
	- TGG was talking about network radios

# 01/06/23

1. KE8TJE
2. KC8YUI
3. WB1AAL

- end of net: 2115 h
- comments:
	- Jim wants to know who else is coming and what bands we wanna operate. @Ian and @Aidan can you guys confirm availability
	- Jeff asked for 40 m (mornings) ARC 40 m Nights

# 08/06/23

1. KE8TJE
2. KC8YUI
3. WB1AAL
4. WA8TGG
5. N8HGL - Bob 

- end of netL 2114 h
- comments:
	- testing 17th at Saberton 9.00 AM meeting. Testing start at ~ 10.30 AM 
	- Messed up calls :P TGG and AAL

# 15/06/23

1. KE8TJE - NC
2. KA3WVU
3. WC0LE
4. WB1AAL
5. WA8TGG
6. KC8YUI
7. N3AMK

- end of net: 2115 h
- comments: 
	- generator status @Tyson
	- other clubs want to get it to field day ? Are we good on this. I know you were planning on some thing on early June
	- key box combo needed. 

# 22/06/23

1. KE8TJE
2. N8OZY
3. KC8YUI
4. WB1AAL

- Not complete but ran the net. Logs recorded in the log book at the shack 

# 29/06/23

1. KE8TJE
2. KB8CFD
3. KC8YUI
4. KE8VEQ
5. N8HGL
6. WB1AAL
7. WA8TGG

- comments:
	- wirelessx ran well
	- WVU core arboretum Talks. Tuesday 6 PM
	- Rocketry launch details
  - Balloon project updates
  - SSB 144.210 M
- end of net: 2119 h

# 06/07/23

1. KE8TJE
2. KB8CFD
3. N8HGL
4. WA8TGG
5. WB1AAL 

- comments: 
	- DMR repeater is not operational (AAL)
	- GRUSK - club is helping on that and we are going out 
- end of net: 2116 h

# 13/07/23

1. KE8TJE
2. KC8YUI
3. WA8TGG
4. WB1AAL
5. KE8VEQ

- comments:
	- DMR repeater timed out after ~ 30 mins
	- 15th MCR meeting 
	- Field days numbers are being compiled
- end of net: 2115 h
- post net chat group: Jeff, Jim ,Elizabeth and Hasith (Rockets and Apollo)

# 20/07/23

1. KE8TJE (NC)
2. KE8VEQ (ANC)
3. KB8CFD
4. WB1AAL
5. WC0LE
6. WA8TGG

- comments: Nothing to add
	- bad signal reports on NC
	- first meeting 31st Aug
	- echolink is up and running.
	- moth night 25th July at core arboretum
- end of net: 2118 h
- post net chat: Jeff,Elizabeth,Hasith - Rockets

# 27/07/23

1. KE8TJE (NC)
2. KE8VEQ
3. WB1AAL
4. KE8LMT (possible new station: Mike)
5. N8HGL
6. N1LVI echolink (partial checkin,audio issue on my end)

- no comments
- end of net: 2110 h

# 03/08/23

1. KE8TJE (NC)
2. KA3WVU
3. N0IAN
4. KB8CFD
5. N8HGL
6. WN8TYC - Tye,Reedsvil MCR member 
7. N1LVI - Echolink

- No comments every one is doing well
	- Echo  link was better. audio drops are there. (likely network issue on user end)
	- Need a modified process for echolink stations
- end of net: 2115 h
- need a net controller next week


# 10/08/23

1. KE8VEQ (NC)
2. N0IAN (ANC)
3. WC0LE
4. KB8CFD
5. WV8EHB
6. KE8WZW
7. N1LVI (e)
8. KE8TJE (e)
9. N3AMK

- comments:
	- First meeting 31st Aug
  - Echolink call outs during net needs to be disabled
 
- end of net: 2124 h

---

# Template to fill for each net<Date>

1. List of station

- Comments: 
	- sub comments
- end of net: <>h



